CFLAGS=\
	-DGST_PACKAGE='"GStreamer"' -DGST_ORIGIN='"http://gstreamer.net"' \
	-DVERSION='"0.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	`pkg-config --cflags gstreamer-0.10` -I../
	
LDFLAGS=`pkg-config --libs gstreamer-0.10`

libgstptsdump.la:gstptsdump.lo
	libtool --mode=link gcc -export-symbols-regex gst_plugin_desc -o $@ $+ $(LDFLAGS) -module -shared -avoid-version -rpath /usr/lib64/gstreamer-0.10/

%.lo: %.c
	libtool --mode=compile gcc $(CFLAGS) -o $@ -c $<
	
.PHONY: install

install: .libs/libgstptsdump.so
	libtool --mode=install install $< /usr/lib64/gstreamer-0.10/

clean:
	rm -rf *.o *.lo *.a *.la .libs
