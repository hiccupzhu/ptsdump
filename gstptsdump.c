/**
 * SECTION:element-ptsdump
 *
 * FIXME:Describe ptsdump here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! ptsdump ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include <stdio.h>
#include <string.h>

#include "gstptsdump.h"

GST_DEBUG_CATEGORY_STATIC (gst_pts_dump_debug);
#define GST_CAT_DEFAULT gst_pts_dump_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT,
  PROP_LOCATION
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

GST_BOILERPLATE (GstPtsDump, gst_pts_dump, GstElement,
    GST_TYPE_ELEMENT);

static void gst_pts_dump_finalize(GObject * object);

static void gst_pts_dump_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_pts_dump_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_pts_dump_set_caps (GstPad * pad, GstCaps * caps);
static GstFlowReturn gst_pts_dump_chain (GstPad * pad, GstBuffer * buf);

/* GObject vmethod implementations */

static void
gst_pts_dump_base_init (gpointer gclass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (gclass);

  gst_element_class_set_details_simple(element_class,
    "PtsDump",
    "FIXME:Generic",
    "FIXME:Generic Template Element",
    "szhu_mail@163.com");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));
}

/* initialize the ptsdump's class */
static void
gst_pts_dump_class_init (GstPtsDumpClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->finalize = gst_pts_dump_finalize;

  gobject_class->set_property = gst_pts_dump_set_property;
  gobject_class->get_property = gst_pts_dump_get_property;

  g_object_class_install_property (gobject_class, PROP_SILENT,
          g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
                  FALSE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_LOCATION,
          g_param_spec_string("location", "location", "dump file name.",
                  "ptsdump.txt", G_PARAM_READWRITE));
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_pts_dump_init (GstPtsDump * filter,
    GstPtsDumpClass * gclass)
{
  filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_setcaps_function (filter->sinkpad,
                                GST_DEBUG_FUNCPTR(gst_pts_dump_set_caps));
  gst_pad_set_getcaps_function (filter->sinkpad,
                                GST_DEBUG_FUNCPTR(gst_pad_proxy_getcaps));
  gst_pad_set_chain_function (filter->sinkpad,
                              GST_DEBUG_FUNCPTR(gst_pts_dump_chain));

  filter->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  gst_pad_set_getcaps_function (filter->srcpad,
                                GST_DEBUG_FUNCPTR(gst_pad_proxy_getcaps));

  gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);
  gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

  filter->silent = FALSE;
  filter->fp = NULL;
  filter->filename = NULL;
}

static void
gst_pts_dump_finalize(GObject * object){
    GstPtsDump *filter = GST_PTSDUMP (object);
    if(filter->filename) free(filter->filename);
    if(filter->fp) fclose(filter->fp);
}

static void
gst_pts_dump_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
    GstPtsDump *filter = GST_PTSDUMP (object);

    switch (prop_id) {
    case PROP_SILENT:
        filter->silent = g_value_get_boolean (value);
        break;
    case PROP_LOCATION:
        if(filter->filename) free(filter->filename);
        filter->filename = strdup(g_value_get_string(value));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gst_pts_dump_get_property (GObject * object, guint prop_id,
        GValue * value, GParamSpec * pspec)
{
    GstPtsDump *filter = GST_PTSDUMP (object);

    switch (prop_id) {
    case PROP_SILENT:
        g_value_set_boolean (value, filter->silent);
        break;
    case PROP_LOCATION:
        g_value_set_string(value, filter->filename);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

/* GstElement vmethod implementations */

/* this function handles the link with other elements */
static gboolean
gst_pts_dump_set_caps (GstPad * pad, GstCaps * caps)
{
  GstPtsDump *filter;
  GstPad *otherpad;

  filter = GST_PTSDUMP (gst_pad_get_parent (pad));
  otherpad = (pad == filter->srcpad) ? filter->sinkpad : filter->srcpad;
  gst_object_unref (filter);

  return gst_pad_set_caps (otherpad, caps);
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_pts_dump_chain (GstPad * pad, GstBuffer * buf)
{
  GstPtsDump *filter;

  filter = GST_PTSDUMP (GST_OBJECT_PARENT (pad));

  if (filter->silent == TRUE)
      return gst_pad_push (filter->srcpad, buf);

  if(!filter->filename){
      char filename[128];
      sprintf(filename, "%s.txt", gst_object_get_name(filter));
      filter->filename = strdup(filename);
  }

  if(!filter->fp) filter->fp = fopen(filter->filename, "w");
  fprintf(filter->fp, "%lld\n", buf->timestamp);
  fflush(filter->fp);

  /* just push out the incoming buffer without touching it */
  return gst_pad_push (filter->srcpad, buf);
}


/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
ptsdump_init (GstPlugin * ptsdump)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template ptsdump' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_pts_dump_debug, "ptsdump",
      0, "ptsdump");

  return gst_element_register (ptsdump, "ptsdump", GST_RANK_NONE,
      GST_TYPE_PTSDUMP);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
 * in configure.ac and then written into and defined in config.h, but we can
 * just set it ourselves here in case someone doesn't use autotools to
 * compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
 */
#ifndef PACKAGE
#define PACKAGE "ptsdump"
#endif

#define VERSION "0.1"

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "ptsdump",
    "Make ptsdump",
    ptsdump_init,
    VERSION,
    "LGPL",
    "GStreamer",
    "http://gstreamer.net/"
)
