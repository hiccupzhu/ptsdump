#ifndef __GST_PTSDUMP_H__
#define __GST_PTSDUMP_H__

#include <gst/gst.h>

G_BEGIN_DECLS

/* #defines don't like whitespacey bits */
#define GST_TYPE_PTSDUMP \
  (gst_pts_dump_get_type())
#define GST_PTSDUMP(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_PTSDUMP,GstPtsDump))
#define GST_PTSDUMP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_PTSDUMP,GstPtsDumpClass))
#define GST_IS_PTSDUMP(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_PTSDUMP))
#define GST_IS_PTSDUMP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_PTSDUMP))

typedef struct _GstPtsDump      GstPtsDump;
typedef struct _GstPtsDumpClass GstPtsDumpClass;

struct _GstPtsDump
{
  GstElement element;

  GstPad *sinkpad, *srcpad;

  gboolean silent;
  FILE *fp;
  gchar *filename;
};

struct _GstPtsDumpClass 
{
  GstElementClass parent_class;
};

GType gst_pts_dump_get_type (void);

G_END_DECLS

#endif /* __GST_PTSDUMP_H__ */
